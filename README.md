SQL is a domain-specific language used in programming and designed for managing data held in a relational database management system, 
or for stream processing in a relational data stream management system.

1. For MySQL install xampp, download first in official site https://www.apachefriends.org
   after install, run xamp control panel, then open localhost in your browser.
   For sqlserver, check out at https://docs.microsoft.com/en-us/sql/database-engine/install-windows/install-sql-server

2. create a database
  
 `CREATE DATABASE dbBelajar`
   
3. create a table
   
  `CREATE TABLE dbBelajar.tblUser (
	id INT (10) AUTO_INCREMENT PRIMARY KEY,
	firstName VARCHAR (30) NOT NULL,
	lastName VARCHAR(30) NOT NULL,
	email VARCHAR(50),
	created_At TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)`

4. Query for delete table: 

`DROP TABLE name_table`
  
 
5. Query insert a record
 
`INSERT INTO tblUser(id,  firstName, lastName, email, created_at)
VALUES ( null, 'NewUser', 'my lastname', 'mail@email.com', null)`

6. Delete a record
   
`DELETE FROM name_table WHERE id = 2 `

7. Update a record

`UPDATE tblUser SET firstName = 'Guest' 
WHERE tblUser.id = 1;`

9. get all list data

`SELECT * FROM tblUser`


10. get one data

`SELECT * FROM tblUser LIMIT 1`

`SELECT TOP 1 * FROM tblUser`

11. get data using clause where
    
`SELECT * FROM tblUser WHERE id=1`

12. query for order data

`SELECT firstName FROM tblUser ORDER BY firstName`

13 query offset, its benefit for pagination (sqlserver)

`SELECT * FROM tblUser 
OFFSET 0 ROWS
FETCH NEXT 10 ROWS ONLY`

14. query get data select top (sqlserver)

`SELECT TOP 1 * FROM tblUser`

`SELECT TOP 50 PERCENT * FROM tblUser`

15. query select distinct

`SELECT DISTINCT firstName FROM tblUser`

16. query caluse LIKE, operator that determines if a character string matches a specified

    `SELECT lastName FROM tblUser WHERE lastName = '%myname%'`
    
17. clause AS (alias)

`SELECT firstName + ' '+ lastName AS FullName FROM tbluser`

18. query inner join, produces a data set that includes rows from the left table which have matching rows from the right table, 
    if any row not match then record rigth table have value null.

    `SELECT * FROM tblUser
LEFT JOIN tblAccess ON tblUser.AccessID = tblAccess.AccessID`

19. The right join returns a result set that contains all rows from the right table and the matching rows in the left table. 
    If a row in the right table that does not have a matching row in the left table, all columns in the left table will contain nulls.

    `SELECT * FROM tblUser
RIGHT JOIN tblAccess ON tblUser.AccessID = tblAccess.AccessID`

20. The full outer join or full join returns a result set that contains all rows from both left and right tables, 
    with the matching rows from both sides where available. In case there is no match, the missing side will have NULL values.


    `SELECT * FROM tblUser
FULL JOIN tblAccess ON tblUser.AccessID = tblAccess.AccessID`



    

